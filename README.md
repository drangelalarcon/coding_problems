# Coding Challenge Problems
This is a collection of some interesting coding challenge problems. I likely solved them in Python and translated the solution into C++ and wrote some test cases for each one.

Don't read the solutions until you have tried it yourself!

## Problems:
[p1. Max/Min Diff From N Resources](#p1)\
[p2. Min Cycles](#p2)

## <a name="p1"></a>p1. Max/Min Diff From N Resources
Given a resource with quantity k, n entities can be created from the resource via the following rules.
1. The first entity must use exactly 1 resource, the second must recieve at least 1.
2. The nth entity must use at least as many as the previous 2 resources.
3. The nth entity must use at least half as many resources as the nth+1 entity.
4. If there are enough resources for another entity, a new entity must be created.

Write a function to find the difference between the max and min number of entities created given k resources.

#### Examples
k = 5, max_n = 3, min_n = 2, diff = 1\
k = 11, max_n = 4, min_n = 3, diff = 1\
k = 143, max_n = 10, min_n = 7, diff = 3

## <a name="p2"></a>p2. Min Cycles
Given a goal state for x and y, find the mininum number of cycles to reach this state with the following rules:
1. x = 1, y = 1 at the start
2. Using one cycle, x can be incremented by y's value
3. Using one cycle, y can be incremented by x's value
4. Return -1 when the goal state is unreachable.

#### Examples
x = 4, y = 7, min_cycles = 4\
x = 2, y = 2, min_cycles = -1
