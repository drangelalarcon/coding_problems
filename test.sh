#!/bin/bash

filename=$1
name=$(echo "$filename" | cut -f 1 -d '.')
test_cases="cases/${name}_cases.cc"
header="cases/${name}.h"
answer="solutions/${name}_answer.cc"
staticlib="temp/${name}.a"

printf %"s\n\n" "Running Test Cases for $name"
mkdir -p temp
g++ -c $filename -o temp/solution.o
g++ -c $answer -o temp/answer.o
{
	ar rs $staticlib temp/solution.o temp/answer.o
} &> /dev/null
g++ -o temp/test $test_cases $staticlib
./temp/test

rm -rf temp
