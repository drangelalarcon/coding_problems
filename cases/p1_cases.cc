#include "p1.h"
#include <iostream>

bool compare_results(int ans, int sol, int test) {
    if (ans == sol) {
        std::cout << "Test Case " + std::to_string(test) + 
                     " Passed! (" + std::to_string(ans) + ")\n";
        return true;
    }
    else {
        std::cout << "Test Case " + std::to_string(test) + 
                     " Failed!\nExpected: " + std::to_string(ans) + 
                     "\nOutput: " + std::to_string(sol) << std::endl;
        return false;
    }
}

bool test_input(int input) {
    int ans = answer(input);
    int sol = solution(input);
    return compare_results(ans, sol, input);
}

void run_cases() {
    int cases[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                   11, 12, 13, 14, 15, 16, 17, 19, 20,
                   142, 143, 1000};
    int len = sizeof(cases)/sizeof(cases[0]);
    bool cont = true;
    int i = 0;

    while (cont) {
        cont = test_input(cases[i]);
        i++;
        cont = cont & (i<len);
    }
}

int main() {
    run_cases();
    return 0;
}