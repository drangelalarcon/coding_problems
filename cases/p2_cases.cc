#include "p2.h"
#include <iostream>

bool compare_results(int ans, int sol, int x, int y) {
    if (ans == sol) {
        std::cout << "Test Case (" + std::to_string(x) + ", " +
                     std::to_string(y) +
                     ") Passed! (" + std::to_string(ans) + ")\n";
        return true;
    }
    else {
        std::cout << "Test Case (" + std::to_string(x) + ", " +
                     std::to_string(y) +
                     ") Failed!\nExpected: " + std::to_string(ans) + 
                     "\nOutput: " + std::to_string(sol) << std::endl;
        return false;
    }
}

bool test_input(int x, int y) {
    int ans = answer(x, y);
    int sol = solution(x, y);
    return compare_results(ans, sol, x, y);
}

void run_cases() {
    int cases[] = {1, 1, 1, 2, 2, 1, 3, 7, 3, 6, 4, 7,
                   12, 17, 44, 123, 85, 2, 357687643, 97987498,
                   200002051, 2000};
    int len = sizeof(cases)/sizeof(cases[0]);
    bool cont = true;
    int i = 0;

    while (cont) {
        cont = test_input(cases[i], cases[i+1]);
        i = i+2;
        cont = cont & (i<len);
    }
}

int main() {
    run_cases();
    return 0;
}