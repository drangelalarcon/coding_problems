int min_entities(int n) {
	int ent = 1;
	int next_min = 2;

	while (n >= next_min) {
		ent += 1;
		next_min = (1 << ent) - 1 + (1 << (ent-1)) + (1 << (ent-2));
	}

	return ent;
}

int max_entities(int n) {
	int n1 = 0;
	int n2 = 1;
	int fib_sum = 1;
	int ent = 0;

	while (fib_sum <= n) {
		n2 = n1 + n2;
		n1 = n2 - n1;
		fib_sum = fib_sum + n2;
		ent++;
	}

	return ent;
}

int answer(int n) {
	if (n == 0) {
		return 0;
	}

    return max_entities(n) - min_entities(n);
}