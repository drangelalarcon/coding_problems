int answer(int x, int y) {
	int large, small, incr, depth = 0;
	if (x > y) {
		large = x;
		small = y;
	}
	else {
		large = y;
		small = x;
	}

	while (small != 1) {
		incr = ((large-small)/small)+1;
		large = large-small*incr;
		depth = depth + incr;

		if (small > large) {
			large = large + small;
			small = large - small;
			large = large - small;
		}

		if (small < 1) {
			return -1;
		}
	}
	
	depth = depth + large - small;
	return depth;
}